import { Injectable, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/users/user.entity';
import { Repository } from 'typeorm';
import { CreateAuthDto } from './dto/create-auth.dto';
import { RegisterAuthDto } from './dto/register-auth.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';
import { hash, compare } from 'bcrypt';
import { LoginAuthDto } from './dto/login-auth.dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
    private jwtService: JwtService,
  ) {}
  create(createAuthDto: CreateAuthDto) {
    return 'This action adds a new auth';
  }

  findAll() {
    return `This action returns all auth`;
  }

  findOne(id: number) {
    return `This action returns a #${id} auth`;
  }

  update(id: number, updateAuthDto: UpdateAuthDto) {
    return `This action updates a #${id} auth`;
  }

  remove(id: number) {
    return `This action removes a #${id} auth`;
  }

  async register(user: RegisterAuthDto) {
    const { password } = user;
    const plaintToHash = await hash(password, 10);
    user = { ...user, password: plaintToHash };
    const newUser = this.userRepository.create(user);
    return this.userRepository.save(newUser);
  }

  async login(user: LoginAuthDto) {
    const { email, password } = user;
    const findUser = await this.userRepository.findOne({
      where: {
        email,
      },
    });
    if (!findUser) throw new HttpException('User not found', 404);
    const checkPassword = await compare(password, findUser.password);
    if (!checkPassword) throw new HttpException('Incorrect password', 403);
    const payload = { id: findUser.id, username: findUser.username };
    const token = await this.jwtService.sign(payload);
    const data = {
      user: findUser,
      token,
    };
    return data;
  }
}
